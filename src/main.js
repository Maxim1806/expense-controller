import Vue from 'vue'
import App from './App.vue'
import '@/scss/index.scss';
import BootstrapVue from 'bootstrap-vue'
import PortalVue from 'portal-vue'
import router from './router'
import store from './store'

Vue.use(BootstrapVue)
Vue.use(PortalVue)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
