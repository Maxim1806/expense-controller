import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [

  {
    path: '/',
    name: 'home',
    component: () => import('@/views/Home')
  },
  {
    path: '/categories',
    name: 'categories',
    component: () => import('@/views/Categories')
  },
  {
    path: '/transactions',
    name: 'transactions',
    component: () => import('@/views/Transactions')
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router