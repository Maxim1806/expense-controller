import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({

  state: {
    balance: 0,
    transactions: [],
    categories: [
      {
        id: 1,
        name: 'Продукты',
        type: 'expense'
      },
      {
        id: 2,
        name: 'Одежда',
        type: 'expense'
      },
      {
        id: 3,
        name: 'Развлечения',
        type: 'expense'
      },
      {
        id: 4,
        name: 'Зарплата',
        type: 'income'
      },
      {
        id: 5,
        name: 'Личные проекты',
        type: 'income'
      },
      {
        id: 6,
        name: 'Иные доходы',
        type: 'income'
      }
    ]
  },

  mutations: {
    SET_TRANSACTION (state, payload) {
      let found = state.transactions.find(transaction => transaction.id === payload.id)

      if (!found) {
        state.transactions.push(payload)
      }
    },
    SET_BALANCE (state, balance) {
      state.balance = balance
    },
    REDUCE_BALANCE (state, id) {
      let balance = state.balance
      const transactions = state.transactions.filter(transaction => transaction.categoryId === id)
      const amountMoney = transactions.map(transaction => transaction.amountMoney).reduce((acc, item) => acc + item, 0)
      state.balance = balance - amountMoney
    },
    INCREASE_BALANCE (state, id) {
      let balance = state.balance
      const transactions = state.transactions.filter(transaction => transaction.categoryId === id)
      const amountMoney = transactions.map(transaction => transaction.amountMoney).reduce((acc, item) => acc + item, 0)
      state.balance = balance + amountMoney
    }
  },

  actions: {
    setBalance (context, payload) {
      context.commit('SET_BALANCE', payload)
    }
  },

  getters: {
    getSumByCategoryId: state => id => {
      const transactions = state.transactions.filter(transaction => transaction.categoryId === id)

      return transactions.map(transaction => transaction.amountMoney).reduce((acc, item) => acc + item, 0)

    },
    getCategoryById: state => id => {
      return state.categories.find(category => category.id === id)
    }
  },

  modules: {}
})
